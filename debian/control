Source: lomiri-system-settings
Section: misc
Priority: optional
Maintainer: Debian UBports Team <team+ubports@tracker.debian.org>
Uploaders:
 Marius Gripsgard <marius@ubports.com>,
 Mike Gabriel <sunweaver@debian.org>,
Build-Depends: cmake,
               cmake-extras,
               dbus-test-runner,
               debhelper-compat (= 13),
               gir1.2-glib-2.0 <!nocheck>,
               intltool,
               libaccountsservice-dev,
               libapt-pkg-dev,
               libclick-dev,
               libdeviceinfo-dev,
               libevdev-dev,
               libgeonames-dev,
               libglib2.0-dev (>= 2.37.92),
               libgnome-desktop-3-dev,
               libgsettings-qt-dev,
               libicu-dev,
               libpolkit-agent-1-dev,
               libqmenumodel-dev,
               libqt5sql5-sqlite <!nocheck>,
               libqtdbusmock1-dev (>= 0.2+14.04.20140724) <!nocheck>,
               libqtdbustest1-dev <!nocheck>,
               libsystemd-dev,
               libudev-dev,
               libupower-glib-dev,
               pkgconf,
               qml-module-lomiri-components <!nocheck>,
               qml-module-lomiri-settings-components <!nocheck>,
               qml-module-lomiri-settings-fingerprint <!nocheck>,
               qml-module-lomiri-settings-menus <!nocheck>,
               qml-module-lomiri-settings-vpn <!nocheck>,
               qml-module-ofono (>=0.117~) <!nocheck>,
               qml-module-qtcontacts,
               qml-module-qtquick-layouts,
               qml-module-qtquick2 <!nocheck>,
               qml-module-qtsysteminfo (>= 5.0~),
               qml-module-qttest <!nocheck>,
               qtbase5-dev,
               qtbase5-private-dev <!nocheck>,
               qtdeclarative5-dev,
               qtdeclarative5-dev-tools,
               suru-icon-theme,
               xvfb <!nocheck>,
               xauth <!nocheck>,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://gitlab.com/ubports/development/core/lomiri-system-settings
Vcs-Git: https://salsa.debian.org/ubports-team/lomiri-system-settings.git
Vcs-Browser: https://salsa.debian.org/ubports-team/lomiri-system-settings

Package: lomiri-system-settings
Architecture: any
Depends: accountsservice,
         bluez (>= 5.23),
         click | ubuntu-snappy-cli,
         gir1.2-glib-2.0,
         gsettings-desktop-schemas,
         fonts-noto-core,
         liblomirisystemsettings1 (= ${binary:Version}),
         lomiri-indicator-network,
         lomiri-schemas (>= 0.1.5),
         python3,
         python3-gi,
         lomiri-keyboard,
         qmenumodel-qml,
         qml-module-gsettings1.0,
         qml-module-lomiri-components,
         qml-module-lomiri-components-extras,
         qml-module-lomiri-connectivity (>= 0.7.1),
         qml-module-lomiri-content,
         qml-module-lomiri-settings-components,
         qml-module-lomiri-settings-fingerprint,
         qml-module-lomiri-settings-menus,
         qml-module-lomiri-settings-vpn,
         qml-module-ofono (>=0.117~),
         qml-module-qt-labs-folderlistmodel,
         qml-module-qtmultimedia | qml-module-qtmultimedia-gles,
         qml-module-qtsysteminfo,
         suru-icon-theme (>= 14.04+15.04.20150813~),
         lomiri-wallpapers,
         upower,
         ${misc:Depends},
         ${shlibs:Depends},
Suggests: aethercast,
          dbus-property-service,
          system-image-dbus (>= 3.1),
          urfkill,
Recommends: lomiri (>= 0.3.0-1~),
            lomiri-sounds,
            lomiri-system-settings-online-accounts,
            repowerd,
Breaks: lomiri-system-settings-security-privacy,
Replaces: lomiri-system-settings-security-privacy,
Provides: lomiri-system-settings-security-privacy,
Description: System Settings application for Lomiri
 Lomiri-system-settings is the System Settings application used in Lomiri
 operating environment. it's designed for phones, tablets and convergent
 devices.

Package: liblomirisystemsettings1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: System Settings application for Lomiri - plug-in library
 Lomiri-system-settings is the System Settings application used in Lomiri
 operating environment. it's designed for phones, tablets and convergent
 devices.
 .
 This package contains the library used by settings plugins.

Package: liblomirisystemsettings-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: liblomirisystemsettings1 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: System Settings application for Lomiri - plug-in development files
 Lomiri-system-settings is the System Settings application used in Lomiri
 operating environment. it's designed for phones, tablets and convergent
 devices.
 .
 This package contains the plug-in library's development files.

Package: liblomirisystemsettingsprivate0.0
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: System Settings application for Lomiri - private library
 Lomiri-system-settings is the System Settings application used in Lomiri
 operating environment. it's designed for phones, tablets and convergent
 devices.
 .
 This package contains the private library used by some settings plugins.

Package: liblomirisystemsettingsprivate-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: liblomirisystemsettingsprivate0.0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: System Settings application for Lomiri - private development files
 Lomiri-system-settings is the System Settings application used in Lomiri
 operating environment. it's designed for phones, tablets and convergent
 devices.
 .
 This package contains the private library's development files.

#Package: lomiri-system-settings-autopilot
#Architecture: all
#Depends: dpkg-dev,
#         gir1.2-upowerglib-1.0,
#         libautopilot-qt,
#         lomiri-system-settings,
#         lomiri-ui-toolkit-autopilot,
#         python3-autopilot,
#         python3-dateutil,
#         python3-dbusmock (>= 0.14),
#         python3-evdev,
#         ${misc:Depends},
#         ${python3:Depends}
#Description: System Settings application for Lomiri - Autopilot tests
# Lomiri-system-settings is the System Settings application used in Lomiri
# operating environment. it's designed for phones, tablets and convergent
# devices.
# .
# This package contains the autopilot tests for lomiri-system-settings.
