lomiri-system-settings (1.3.0-4) unstable; urgency=medium

  * debian/control:
    + Drop python3* build-dependencies. Not needed (autopilot remnants from
      upstream packaging).
    + Pull-in lomiri-keyboard rather than maliit-keyboard.
  * debian/patches:
    + Drop 2000_use-maliit-keyboard-for-language-plugin.patch. Use lomiri-
      keyboard from now on.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 26 Jan 2025 12:27:05 +0100

lomiri-system-settings (1.3.0-3) unstable; urgency=medium

  * debian/control:
    + Drop version from libicu-dev in B-D: field. This was for testing and
      should not have landed in the uploaded package.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 10 Jan 2025 09:58:47 +0100

lomiri-system-settings (1.3.0-2) unstable; urgency=medium

  * debian/patches:
    + Add 1001_dont-hardcode-cxx-standard.patch. (Closes: #1092339).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 07 Jan 2025 17:48:05 +0100

lomiri-system-settings (1.3.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 07 Jan 2025 17:20:27 +0100

lomiri-system-settings (1.2.0-2) unstable; urgency=medium

  * debian/rules:
    + Fix sloppy usage of HOME vs. BUILDHOME and its removal during
      dh_auto_clean override. (Closes: #1090244).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 17 Dec 2024 13:15:16 +0100

lomiri-system-settings (1.2.0-1) unstable; urgency=medium

  * New upstream release.
    - Ship l-s-s-security-privacy settings dialogs.
  * debian/control:
    + Add B:/R: for lomiri-system-settings-security-privacy.
    + Bump Standards-Version to 4.7.0. No changes needed.
    + Recommend lomiri >= 0.3.0.
    + Adjust to merged-in security-privacy plugin.
    + Require lomiri-schemas (>= 0.1.5).
    + Add to D: python3, python3-gi (for update-notification-settings script).
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/patches:
    + Drop all patches. Applied upstream.
    + Rebase 2000_use-maliit-keyboard-for-language-plugin.patch.
    + Add 2011_build-without-trust-store.patch and 2012_no-Location-service-
      yet.patch. Build without trust-store and location-service support for
      now.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 17 Jul 2024 12:45:36 +0200

lomiri-system-settings (1.1.0-4) unstable; urgency=medium

  [ Guido Berhoerster ]
  * debian/<systemd-magic>: Add script and systemd path unit for updating
    notifications preferences:
    This watches /usr/lib/lomiri-push-service/legacy-helpers/ for changes and
    updates the GSettings com.lomiri.notifications.settings.applications setting
    accordingly.

  [ Mike Gabriel ]
  * debian/rules:
    + Amend dh_makeshlibs override, use correct path.
  * debian/lomiri-system-settings-notification-settings.{path,service}:
    + Only update applications via lomiri-system-settings-notification-
      settings.service on lomiri session start.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 22 May 2024 12:03:27 +0200

lomiri-system-settings (1.1.0-3) unstable; urgency=medium

  * debian/patches:
    + Add patches 0006a - 0006c. Support using vendor interface in
      AccountsService for storing background image path.
    + Drop 2001_disable-current-language-switching.patch. Handled
      by upstream commit 722fc6ce.
  * debian/rules:
    + Build with -DENABLE_UBUNTU_ACCOUNTSSERVICE=ON only on Ubuntu and
      derivatives.
  * debian/watch:
    + Update file for recent GitLab changes of the tags overview page.
  * debian/control:
    + Recommend lomiri (>= 0.2.1-11~) which has the required AccountsService
      property for patch 0006a.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 21 Apr 2024 10:58:53 +0200

lomiri-system-settings (1.1.0-2) unstable; urgency=medium

  * debian/control:
    + Update LONG_DESCRIPTION fields. Lomiri is not a desktop environment, but
      an operating environment.
    + Add to D: lomiri-system-settings-security-privacy. This introduces a
      cyclic dependency between l-s-s and l-s-s-security-privacy. The
      battery plugin in l-s-s requires QML code from l-s-s-security-privacy
      while l-s-s-security-privacy requires the l-s-s UI for being displayed.
  * debian/patches:
    + Drop 2003_hide-lock-on-suspend-or-sleep.patch. lomiri-system-settings-
      security-privacy is now available in Debian.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 26 Mar 2024 21:52:15 +0100

lomiri-system-settings (1.1.0-1) unstable; urgency=medium

  * debian/patches:
    + Add 1004_qofono-module-notifications-plugin.patch. Fix not displayed
      notifications plugin (wrong module path for QOfono QML module).
    + Add 2003_hide-lock-on-suspend-or-sleep.patch. Comment lock-on-suspend /
      sleep-when-idle setting until l-s-s-security-privacy has been uploaded
      to Debian.
  * debian/control:
    + Update versioned D on qml-module-ofono to (>= 0.117~).
    + Add to B-D: (for unit tests): qml-module-ofono (>=0.117~).
    + Switch from pkg-config to pkgconf. Thanks, lintian.
  * debian/{control,rules}:
    + B-D and D clean-up. Drop various Python dependencies.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 11 Mar 2024 12:33:08 +0100

lomiri-system-settings (1.0.2-2) unstable; urgency=medium

  * debian/control:
    + Add to R: lomiri-system-settings-online-accounts.
    + Add to D: upower.
    + Add to D: qml-module-lomiri-settings-fingerprint.
    + Add to B-D: libdeviceinfo-dev.
    + Add to B-D: qml-module-lomiri-settings-fingerprint (for unit tests).
    + Drop from B-D: gdb.
  * debian/rules:
    + Build with -DENABLE_LIBDEVICEINFO=ON.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 05 Mar 2024 09:18:05 +0100

lomiri-system-settings (1.0.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update copyright attribution for debian/.
    + Update copyright attributions.
  * debian/patches:
    + Rebase 0002_bt-test-fix-with-modern-python-dbusmock.patch and 2002_use-
      Noto-font-instead-of-Ubuntu-font.patch.
    + Add 1003_bt-test-fix-with-modern-python-dbusmock-since-30.2.patch. Fix
      build against python-dbusmock 0.30.2 and beyond. (Closes: #1060992).
    + Add patch header to 2002_use-Noto-font-instead-of-Ubuntu-font.patch.
    + Rename 1001_use-maliit-keyboard-for-language-plugin.patch to 2000_use-
      maliit-keyboard-for-language-plugin.patch (i.e. consider patch Debian-
      specific).
  * debian/control:
    + Bump versiond B-D on python3-dbusmock to 0.30.2.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 01 Feb 2024 19:37:30 +0000

lomiri-system-settings (1.0.1-2) unstable; urgency=medium

  * debian/control:
    + Add to D (l-s-s): fonts-noto-core.
  * debian/patches:
    + Add 2002_use-Noto-font-instead-of-Ubuntu-font.patch.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Mar 2023 00:03:40 +0100

lomiri-system-settings (1.0.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop 2001_no_language_plugin.patch. Add 1001_use-maliit-keyboard-for-
      language-plugin.patch instead.
    + Add 2001_disable-current-language-switching.patch. Disable instantaneous
      language switching as this is not supported in Debian's AccountsService.
  * debian/control:
    + Add to D: maliit-keyboard (and drop commented out lomiri-keyboard-data).
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 24 Feb 2023 08:18:33 +0100

lomiri-system-settings (1.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/watch:
    + Start watching upstream releases.
  * debian/copyright:
    + Update auto-generated copyright.in file.
  * debian/control:
    + Bump Standards-Version: to 4.6.2. No changes needed.
  * debian/copyright:
    + Update copyright attributions.
  * debian/rules:
    + Don't update pot file during build. (This should never have landed here!).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 06 Feb 2023 14:21:04 +0100

lomiri-system-settings (1.0~git20221229.bc061a4-1) unstable; urgency=medium

  * New upstream Git snapshot.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/patches:
    + Drop patch 2002 in favour of patches 000{1,2}_bt-test-fix-with-modern-
      python-dbusmock.patch.
  * debian/rules:
    + Enable 'MODERN_PYTHON_DBUSMOCK' CMake option.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 02 Jan 2023 12:57:25 +0100

lomiri-system-settings (1.0~git20221028.ca6da85-1) unstable; urgency=medium

  [ Mike Gabriel ]
  * New upstream Git snapshot.
  * debian/patches/:
    + Drop 0001_copyright-headers-Use-uniform-spelling-for-copyright.patch.
      Applied upstream.
  * debian/control:
    + Switch from ubports-wallpapers to lomiri-wallpapers (currently in
      Debian's NEW queue); comment out lomiri-keyboard-data (we don't
      build the language plugin for now). (Closes: #1021524).
  * debian/copyright:
    + Update copyright attributions.

  [ Anton Gladky ]
  * Add debian/.gitlab-ci.yml
  * Let blhc fail

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 28 Oct 2022 22:58:33 +0200

lomiri-system-settings (1.0~git20221004.572f3a9-1) unstable; urgency=medium

  * Initial upload to Debian. (Closes: #1007782).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 05 Oct 2022 17:22:47 +0200
